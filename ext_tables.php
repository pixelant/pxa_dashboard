<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Pixelant Dashboard');

if (TYPO3_MODE === 'BE') {

	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		// 'PixelantAB.' . $_EXTKEY,
		$_EXTKEY,
		'user',	 			// Make module a submodule of 'tools'
		'controlpanel',		// Submodule key
		'',					// Position
		array(
			'ControlPanel' => 'home,task,configuration,statistics,json',
		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/mod_controlpanel.gif',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_controlpanel.xlf',
		)
	);

}
