function getSysActionsList(target) {

	var ulList = jQuery('#' + target);
	ulList.empty();
	var li = jQuery('<li/>').addClass(target + '-item').attr('role', 'menuitem').appendTo(ulList);
	var span = jQuery('<span/>').addClass('icon-spinner icon-spin').text('').appendTo(li);
	
    jQuery.ajax({
        type: 'POST',
        url: getAjaxUrl('actionsList'),
        dataType: 'json',
        success: function(data){
            fillUrlList(data.items,target);
        },
        error: function( jqXHR, textStatus, errorThrown ) {
			ulList.empty();
			var li = jQuery('<li/>').addClass(target + '-item').attr('role', 'menuitem').appendTo(ulList);
			var i = jQuery('<i/>').addClass('icon-warning-sign').appendTo(li);
			var span = jQuery('<span/>').addClass('json-error').text(errorThrown).appendTo(li);
		}
    });
}

function getRecentChangesList(target) {

	var ulList = jQuery('#' + target);
	ulList.empty();
	var li = jQuery('<li/>').addClass(target + '-item').attr('role', 'menuitem').appendTo(ulList);
	var span = jQuery('<span/>').addClass('icon-spinner icon-spin').text('').appendTo(li);
	
    jQuery.ajax({
        type: 'POST',
        url: getAjaxUrl('recentChangesList'),
        dataType: 'json',
        success: function(data){
            fillUrlList(data.items,target);
        },
        error: function( jqXHR, textStatus, errorThrown ) {
        	ulList.empty();
        	var li = jQuery('<li/>').addClass(target + '-item').attr('role', 'menuitem').appendTo(ulList);
        	var i = jQuery('<i/>').addClass('icon-warning-sign').appendTo(li);
        	var span = jQuery('<span/>').addClass('json-error').text(errorThrown).appendTo(li);
        }
    });
}

function getAjaxUrl(parameter) {
	return jsBaseUrl + '&tx_pxadashboard_user_pxadashboardcontrolpanel%5BajaxParam%5D=' + parameter
}
function getExtraParamForAjaxUrl(parameterName, parameterValue) {
	return '&tx_pxadashboard_user_pxadashboardcontrolpanel%5B' + parameterName + '%5D=' + parameterValue
}

function addReturnUrlParameter(parameter) {
	return parameter + '&SET[returnUrl]=' + jsBaseUrl
}

function getFeed(feedName,target) {

	var feedDiv = jQuery('#' + target);
	feedDiv.empty();
	feedDiv.append('<header><i class="icon-spinner icon-spin"></i></header>');
	// var span = jQuery('<span/>').addClass('icon-spinner icon-spin').text('').appendTo(feedDiv);
	
	jQuery.ajax({
        type: 'POST',
        url: getAjaxUrl('getFeed') + getExtraParamForAjaxUrl('feedName',feedName),
        dataType: 'json',
        success: function(data){
            outputFeedAsHtml(data.items, target);
        },
        error: function(){
        	feedDiv.empty();
			feedDiv.append('');
        }
    });
}

function getGoogleAnalyticsData(name,target) {

	jQuery.ajax({
        type: 'POST',
        url: getAjaxUrl('getGoogleAnalyticsData') + getExtraParamForAjaxUrl('name',name),
        dataType: 'json',
        success: function(data){
            if (data.items) {
	        	var dataTable = google.visualization.arrayToDataTable(data.items.dataTable);
	        	var options = data.items.options;
	        	var chartType=data.items.chartType;
	        	var chart = new google.visualization[chartType](document.getElementById(target));
	        	chart.draw(dataTable, options);
	        }
        },
        error: function(){
        	feedDiv.empty();
			feedDiv.append('N/A');
        }
    });
}

function outputFeedAsHtml(feed, target) {

	var feedDiv = jQuery('#' + target);
	feedDiv.empty();
	feedDiv.append('<header><i class="icon-list-alt"></i><span id="feedTitle">' + feed.channel.title + '</span></header>');
	
	jQuery.each( feed.channel.item, function( i, item ) {
		
		var descriptionText = item.description;
		var feedDate = new Date(item.pubDate);

		if (descriptionText.length > 120) {
			descriptionText.substr(0,117) + '...';
		}
		
		var divRow = jQuery('<div></div>').addClass("row-fluid").appendTo(feedDiv);		
		var divFeed = jQuery('<div></div>').addClass("news-container").appendTo(divRow);
		var divCol1 = jQuery('<div></div>').addClass("span10").appendTo(divFeed);
		var titleLink = jQuery('<a></a>').attr('href', item.link).attr('target','_blank').attr('title',item.title).appendTo(divCol1);
		var header = jQuery('<h5></h5>').text(item.title).appendTo(titleLink);
		var description = jQuery('<p></p>').text(descriptionText).appendTo(divCol1);

		var divCol2 = jQuery('<div></div>').addClass("span2").appendTo(divFeed);
		var divDateParent = jQuery('<div></div>').addClass("date-parent").appendTo(divCol2);
		var divDateChild = jQuery('<div></div>').addClass("date-child").appendTo(divDateParent);
		var spanDay = jQuery('<span></span>').addClass("day-number").text(feedDate.getDate()).appendTo(divDateChild);
		var spanMonth = jQuery('<span></span>').addClass("month").text(monthNames[feedDate.getMonth()]).appendTo(divDateChild);

		
	});
}

function fillUrlList(items,target,disableIcons) {

	var ulList = jQuery('#' + target);
	ulList.empty();

	jQuery.each( items, function( i, item ) {
		var jsOnClick = item.onclick ;
		var li = jQuery('<li/>').addClass(target + '-item').attr('role', 'menuitem').appendTo(ulList);
		var icon = (disableIcons !== 1 ? item['icon'] : "");
       	var aaa = jQuery('<a/>').addClass('ui-all').attr('href',item['uri']).attr('onclick', item.onclick).text(item['title']).prepend(icon).appendTo(li);
	});
	
}