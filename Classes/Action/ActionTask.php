<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Pixelant , Pixelant
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Modifief Action Task for rendering record list
 * 
 * @package TYPO3
 * @subpackage pxa_core
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * 
 */
class Tx_PxaDashboard_Action_ActionTask extends \TYPO3\CMS\SysAction\ActionTask {
	/**
	 * @var mixed
	 */
	protected $taskObject;

	/**
	 * Constructor
	 *
	 * @param mixed $taskObject TaskObject to be used
	 * @return void
	 */
	public function __construct($taskObject) {
		$this->taskObject = $taskObject;
		$GLOBALS['LANG']->includeLLFile('EXT:sys_action/locallang.xml');
	}

	/**
	 * Action to create a list of records of a specific table and pid
	 *
	 * @param array $record sys_action record
	 * @return string list of records
	 */
	public function viewRecordList($record) {
		$content = parent::viewRecordList($record);
		return $content;
	}
}
?>