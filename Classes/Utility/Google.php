<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2013 Jan Spisiak <jan@pixelant.se> Pixelant AB
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_dashboard') . 'Resources/Private/Contrib/google-api-php-client/src/Google/autoload.php';
// require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_dashboard') . 'Resources/Private/Contrib/google-api-php-client/src/Google_Client.php';
// require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_dashboard') . 'Resources/Private/Contrib/google-api-php-client/src/contrib/Google_AnalyticsService.php';

/**
 * Check access of the user to display only those actions which are allowed
 * and needed
 *
 * @package TYPO3
 * @subpackage tx_pxadashboard
 */
class Tx_PxaDashboard_Utility_Google {
	
	/**
	 * Client Object
	 * @var Google_Client
	 */
	public $client;

	/**
	 * Analytics Service
	 * @var Google_AnalyticsService
	 */
	public $analytics;

	/**
	 * Initializes Google Api connection
	 * 
	 * @param  string $appName     Aplication Name
	 * @param  string $email       Service Account Email
	 * @param  string $clientId    Service Account Client Id
	 * @param  string $key         Service Account Key
	 * @param  string $keyPassword Key Password if different from default
	 * @param  array  $scopes      Accessibble scopes
	 * @return void
	 */
	public function init($appName, $email, $clientId, $key, $keyPassword, $scopes = NULL) {
			// Default scopes
		if (!isset($scopes)) {
			$scopes = array(
				'https://www.googleapis.com/auth/analytics.readonly'
			);
		}

		$this->client = new Google_Client(array(
			'application_name' => $appName,
			'oauth2_client_id' => $clientId,
		));

		if (!empty($keyPassword)) {
			$this->client->setAssertionCredentials(
				new \Google_Auth_AssertionCredentials($email, $scopes, $key, $keyPassword)
			);
		} else {
			$this->client->setAssertionCredentials(
				new \Google_Auth_AssertionCredentials($email, $scopes, $key)
			);
		}

			// Get accessToken from cache
		$cache = $GLOBALS['typo3CacheManager']->getCache('pxa_dashboard_google');
		$cacheKey = hash('sha1', "Classes/Utility/Google/AccessToken . $clientId");
		$accessToken = $cache->get($cacheKey);

			// Check if token is valid otherwise refresh it
		$refreshToken = true;
		if (!empty($accessToken)) {
			try {
				$this->client->setAccessToken($accessToken);
				$refreshToken = false;
			} catch (Google_AuthException $e) {
				$refreshToken = true;
			}
		}

		if ($refreshToken) {
			$this->client->getAuth()->refreshTokenWithAssertion();
			$accessToken = $this->client->getAccessToken();
			$cache->set($cacheKey,$accessToken,array(),3550);
		}

			// Use objects for API instead of arrays
		// $this->client->setUseObjects(true);
	}

	/**
	 * Initializes Google Api connection
	 * 
	 * @param  string $appName     Aplication Name
	 * @param  string $email       Service Account Email
	 * @param  string $clientId    Service Account Client Id
	 * @param  string $key         Service Account Key
	 * @param  string $keyPassword Key Password if different from default
	 * @param  array  $scopes      Accessibble scopes
	 * @return void
	 */
	public function __construct($appName, $email, $clientId, $key, $keyPassword, $scopes = NULL) {
		$args = func_get_args();
		call_user_func_array(array($this,'init'), $args);
	}

	/**
	 * Returns Analytics Service
	 * 
	 * @return Google_AnalyticsService
	 */
	public function getAnalytics() {
		if (!isset($this->analytics)) {
			$this->analytics = new \Google_Service_Analytics($this->client);
		}
		return $this->analytics;
	}
}

?>