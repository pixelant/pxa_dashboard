<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Pixelant , Pixelant
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Control Panel Controller Class
 * 
 * @package TYPO3
 * @subpackage pxa_dashboard
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * 
 */
class Tx_PxaDashboard_Controller_ControlPanelController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * Document Template
	 * @var \TYPO3\CMS\Backend\Template\DocumentTemplate
	 */
	public $doc;

	/**
	 * Google Analytics Api Object
	 * @var Google_AnalyticsService
	 */
	protected $analytics;

	/**
	 * Array of Disabled partials
	 * @var array
	 */
	protected $disabledPartials;

	/**
	 * Initializes Google Analytics connection
	 * 
	 * @return void
	 */
	public function initAnalytics() {
		$key = base64_decode($this->settings['googleApi']['serviceAccount']['base64Key']);
		if (isset($this->settings['googleApi']['serviceAccount']['privateKey'])) {
			$key = $this->settings['googleApi']['serviceAccount']['privateKey'];
		}
		$this->google = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Tx_PxaDashboard_Utility_Google',
			'Pixelant Analytics Service',
			$this->settings['googleApi']['serviceAccount']['email'],
			$this->settings['googleApi']['serviceAccount']['clientId'],
			$key,
			$this->settings['googleApi']['serviceAccount']['keyPassword']
		);
		$this->analytics = $this->google->getAnalytics();
	}

	/**
	 * Action initialize
	 *
	 * @return void
	 */
	public function initializeAction() {
		$this->doc = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Backend\\Template\\DocumentTemplate');
		if (!isset($GLOBALS['SOBE'])) {
			$GLOBALS['SOBE'] = new stdClass();
		}
		$GLOBALS['SOBE']->doc = $this->doc;
		$this->pageRenderer = $this->doc->getPageRenderer();
		$this->pageRenderer->loadJquery('latest', NULL, \TYPO3\CMS\Core\Page\PageRenderer::JQUERY_NAMESPACE_DEFAULT_NOCONFLICT);
			// Google Analytics Api + Google Charts
		$this->pageRenderer->addHeaderData('<script type="text/javascript" src="https://www.google.com/jsapi?autoload={\'modules\':[{\'name\':\'visualization\',\'version\':\'1\',\'packages\':[\'corechart\',\'table\']}]}"></script>');
			// ControlPanel JS Functions
		$this->pageRenderer->addHeaderData('<script type="text/javascript" src="' . t3lib_extMgm::extRelPath('pxa_dashboard') . 'Resources/Public/Js/Backend/controlPanel.js"></script>');		

			// Add array of monthnames
		$months = 'var monthNames = [';
		for($x = 0; $x < 12; $x++) {
			if ( $x > 0 ) {
				$months .= ',';
			}
			$months .= '"' . date('M', mktime(0, 0, 0, ($x + 1), 1)) . '"';
		}
		$months .= ']';
		$this->pageRenderer->addHeaderData('<script type="text/javascript">' . $months . "</script>");

			// Init Analytics
		try {
			$this->initAnalytics();
		} catch (Google_AuthException $e) {
			$this->disabledPartials[] = 'ControlPanel/GoogleAnalyticsChart';
			\TYPO3\CMS\Core\Messaging\FlashMessageQueue::addMessage(new \TYPO3\CMS\Core\Messaging\FlashMessage('Google Api Auth Exception thrown. Make sure that your service account credentials are correctly set in Typoscript.' . $e->getMessage(), 'Header', \TYPO3\CMS\Core\Messaging\FlashMessage::WARNING));
		}

			// Check if sys_actions is enabled
		$packageManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Package\\PackageManager');
		$isSysActionActive = $packageManager->isPackageActive('sys_action');

		if (!$isSysActionActive) {
			$this->disabledPartials[] = 'ControlPanel/ActionsList';
			\TYPO3\CMS\Core\Messaging\FlashMessageQueue::addMessage(new \TYPO3\CMS\Core\Messaging\FlashMessage('Extension sys_action is not enabled. Enable it to get action entries.', 'Header', \TYPO3\CMS\Core\Messaging\FlashMessage::WARNING));
		}


	}

	/**
	 * Action configuration
	 *
	 * @return void
	 */
	public function configurationAction() {
		$this->genericForAllActions();
	}

	/**
	 * Action home
	 *
	 * @return void
	 */
	public function homeAction() {
		$this->genericForAllActions();
	}

	/**
	 * Action statistics
	 *
	 * @return void
	 */
	public function statisticsAction() {
		$this->genericForAllActions();
	}

	/**
	 * Default for all Actions
	 *
	 * @return void
	 */
	public function genericForAllActions() {
		
			// Check for disabled partials and unset them in settings (controlPanel -> actions -> actionName -> columns -> columnName -> content -> contentName -> partial.
		foreach ($this->settings['controlPanel']['actions'] as $actionMethodName => $actionMethodParams) {
			foreach ($this->settings['controlPanel']['actions'][$actionMethodName]['columns'] as $actionMethodColumn => $actionMethodColumnParams) {
				foreach ($this->settings['controlPanel']['actions'][$actionMethodName]['columns'][$actionMethodColumn]['content'] as $content => $contentParams) {
					$partialName = $this->settings['controlPanel']['actions'][$actionMethodName]['columns'][$actionMethodColumn]['content'][$content]['partial'];
					if (is_array($this->disabledPartials)) {
						if (in_array($partialName, $this->disabledPartials)) {
							unset($this->settings['controlPanel']['actions'][$actionMethodName]['columns'][$actionMethodColumn]['content'][$content]);
						}
					}
				}
			}
		}
		

			// Assign variables to fluid template
		$this->view->assign('currentAction', $this->actionMethodName);
		$this->view->assign('tsLayot', $this->settings['controlPanel']['actions'][$this->actionMethodName]);
		$this->view->assign('tsTabs', $this->settings['controlPanel']['tabs']);
	}

	/**
	 * This action should handle all the tasks related functionality
	 *
	 * @return void
	 */
	public function taskAction() {
		$this->genericForAllActions();
		

		$this->doc->wrapScriptTags(
			$this->doc->redirectUrls()
		);

		$actionId = $this->request->getArgument('show');

		$record = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('sys_action', $actionId);

			// If the action is not found
		if (count($record) == 0) {
			\TYPO3\CMS\Core\Messaging\FlashMessageQueue::addMessage(
				new \TYPO3\CMS\Core\Messaging\FlashMessage(
					$GLOBALS['LANG']->getLL('action_error-not-found', TRUE),
					$GLOBALS['LANG']->getLL('action_error'),
					\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				)
			);
		} else {
				// Output depends on the type
			switch ($record['type']) {
					// Record List 
				case 3:
					$this->view->assign('listTable', $this->getRecordList($record));
					$this->view->assign('listTableTitle', $record['title']);
					break;
					// Edit Content
				case 4:
					$redirectUrl = $this->getEditContentUri($record);
					$this->redirectToURI($redirectUrl, $delay = 0, $statusCode = 303);
					exit;
					break;
					// Create new record
				case 5:
					$returnUrl = rawurlencode($this->uriBuilder->uriFor('home'));
					$link = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_DIR') . $GLOBALS['BACK_PATH'] . 'alt_doc.php?returnUrl=' . $returnUrl . '&edit[' . $record['t3_tables'] . '][' . intval($record['t3_listPid']) . ']=new';
					$this->redirectToURI($link, $delay = 0, $statusCode = 303);
					exit;
					break;
				default:
					\TYPO3\CMS\Core\Messaging\FlashMessageQueue::addMessage(
						new \TYPO3\CMS\Core\Messaging\FlashMessage(
							$GLOBALS['LANG']->getLL('action_noType', TRUE),
							$GLOBALS['LANG']->getLL('action_error'),
							\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
						)
					);
			}
		}
	}

	/**
	 * Action to create a list of records of a specific table and pid
	 *
	 * @param array $record sys_action record
	 * @return string       HTML List
	 */
	protected function getRecordList($record) {
		$actionTask = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Tx_PxaDashboard_Action_ActionTask', $this);
		return $actionTask->viewRecordList($record);
	}

	/**
	 * Action Json
	 * Gets information to "modules" by ajax 
	 * 
	 * @return string
	 */
	public function jsonAction() {
		
		$whatToGet = $this->request->getArgument('ajaxParam');

		switch ($whatToGet) {
			case 'actionsList':
				$dataArray = $this->getActionEntries();		
				break;
			case 'recentChangesList':
				$dataArray = $this->getRecentlyChanged(' AND sys_log.userid=' . $GLOBALS['BE_USER']->user['uid']);
				break;
			case 'getFeed':
				$feedName = $this->request->getArgument('feedName');
				$dataArray = $this->getFeed($feedName);
				break;		
			case 'GAVisitorsData':
				$dataArray = $this->getGoogleAnalyticsVisitorsData();
				break;				
			case 'GAVisitsByCountry':
				$dataArray = $this->getGoogleAnalyticsVisitsByCountryData();
				break;				
			case 'GAVisitsByBrowser':
				$dataArray = $this->getGoogleAnalyticsVisitsByBrowserData();
				break;
			case 'getGoogleAnalyticsData':
				$name = $this->request->getArgument('name');
				$dataArray = $this->getGoogleAnalyticsData($name);
				break;			
			default:
				$dataArray = array();
				break;
		}
		
		foreach ($dataArray as $key => $data) {
			if (is_array($data)) {
				foreach ($data as $column => $value) {
					$items[$key][$column] = $value;
				}
			} else {
				$items[$key] = $data;
			}
			
		}

		return json_encode(array('items' => $items,'result' => 'true', 'count' => count($items)));

	}

	/**
	 * Gets getGoogleAnalyticsData
	 *
	 * @param string $name of typoscript configuration
	 * @return array Array of action menu entries
	 */
	protected function getGoogleAnalyticsData($name) {
			
			// get ts configuration by name
		$gaConf = $this->settings['controlPanel']['googleAnalytics'][$name];

			// Exit if no configuration exists
		if (!isset($gaConf)) {
			return false;
		}
		if (!isset($this->analytics)) {
			return false;
		}
			// check analytics settings
		if (empty($this->settings['googleApi']['analytics']['webPropertyId'])) {
			$webPropertyId = $this->analytics->management_webproperties->listManagementWebproperties($this->settings['googleApi']['analytics']['accountId'])->getItems();
			$webPropertyId = $webPropertyId[0]->id;
		} else {
			$webPropertyId = $this->settings['googleApi']['analytics']['webPropertyId'];
		}
		$profileId = $this->analytics->management_profiles->listManagementProfiles($this->settings['googleApi']['analytics']['accountId'], $webPropertyId)->getItems();
		$profileId = $profileId[0]->id;

			// Set variables from configuration
		$periodFrom = $gaConf['strtotime']['from'];
		$periodTo = $gaConf['strtotime']['to'];
		$metrics = $gaConf['metrics'];
		$optParams = $gaConf['optParams'];
		$headers[0] = $gaConf['headers'];
		$options = $gaConf['options'];
		$chartType = $gaConf['chartType'];
		$converts = $gaConf['converts'];
		$reArrangeDataByDate = $gaConf['reArrangeDataByDate'];

		$dateFrom = new DateTime(date('Y-m-d', strtotime($periodFrom)));
		$dateTo = new DateTime(date('Y-m-d', strtotime($periodTo)));

			// Fetch the data from ga
		$data = $this->analytics->data_ga->get(
			'ga:' . $profileId,
			$dateFrom->format("Y-m-d"),
			$dateTo->format("Y-m-d"),
			$metrics,
			$optParams
		);	
		
			// get rows to array
		$rows = $data->getRows();

			// Any converts that needs to be done before returning values as json
		foreach ($converts as $column => $type) {
			if (is_array($rows)) {
				foreach ($rows as $key => $data) {
					switch ($type) {
						case 'int':
							$rows[$key][$column] = (int)$data[$column];
							break;
						default:
							$rows[$key][$column] = $data[$column];
							break;
					}
				}
			}
		}

			// Arrange array of Google data by date
		if (isset($reArrangeDataByDate)) {
			$rows = $this->orderGoogleAnalyticsDataByDates(
				$rows,
				$reArrangeDataByDate['yearIndex'],
				$reArrangeDataByDate['monthIndex'],
				$reArrangeDataByDate['dayIndex'],
				$reArrangeDataByDate['indexValuesToBuild']
			);
				// echo "reArrangeDataByDate = " . var_dump($rows) . " ( " . var_dump($reArrangeDataByDate) . ")";

				// create variables to use when building an array with all dates, even if no data exists for the date.
			$dateTemp = $dateFrom;
			$arrayKey = 1;
			$tempArray = array();
		
				// building an array with all dates, even if no data exists for the date.
			while ($dateTemp <= $dateTo) {
					// Set variables to get values in restructured data
				$year = $dateTemp->format('Y');
				$month = $dateTemp->format('m');
				$day = $dateTemp->format('d');
					
					// Restructure rows to date in first column and the data in the other columns, column 0 is always the date
				$tempArray[$arrayKey][0] = $dateTemp->format($reArrangeDataByDate['mappings']['dateFormat']);
					// Go thru "mapping-columns"
				foreach ($reArrangeDataByDate['mappings']['columns'] as $key => $value) {
						// echo ("key = " . $key . " value[key] = " . $value['key']);
					if ( isset($rows[$year][$month][$day][$value['key']] )) {
						$tempArray[$arrayKey][$key] = $rows[$year][$month][$day][$value['key']];
					} else {
						$tempArray[$arrayKey][$key] = 'N/A (key=' . $value['key'] . ')';
					}
					
				}
					// Update temp date
				$dateTemp->add(new DateInterval('P1D'));
				$arrayKey++;
			}
			$rows = $tempArray;
		}
		if (is_array($rows)) {
			$dataTableArray = array_merge($headers, $rows);
		} else {
			$dataTableArray = $headers;
		}

		return array('chartType' => $chartType, 'options' => $options,'dataTable' => $dataTableArray);
		
	}

	/**
	 * Try to find a value by a date and filter and return the value from a ga return array
	 * dimensions: ga:year, ga:month, ga:day must be present so we can find the correct date
	 *
	 * @param array $rows the data to sort
	 * @param integer $yearIndex the index of the year
	 * @param integer $monthIndex the index of the month
	 * @param integer $dayIndex the index of the day
	 * @param array $indexValuesToBuild array of indexes for 'key' => 'values' to build for each date
	 * @return array
	 */	
	protected function orderGoogleAnalyticsDataByDates($rows, $yearIndex, $monthIndex, $dayIndex, $indexValuesToBuild) {

		$returnArray = array();

		if (is_array($rows)) {
			foreach ($rows as $key => $values) {
				foreach ($indexValuesToBuild as $buildIndex => $buildValue) {
					$year = $values[$yearIndex];
					$month = $values[$monthIndex];
					$day = $values[$dayIndex];
					$index = $values[intval($buildIndex)];
					$value = $values[intval($buildValue)];
					$returnArray[$year][$month][$day][$index] = $value;
				}
			}
		}
		return $returnArray;
	}


	/**
	 * Gets the entries for the action menu
	 *
	 * @return array Array of action menu entries
	 */
	protected function getActionEntries() {

		if (is_array($this->disabledPartials)) {
			if (in_array('ControlPanel/ActionsList', $this->disabledPartials)) {
				return false;
			}
		}

		$actions = array();
		if ($GLOBALS['BE_USER']->isAdmin()) {
			$queryResource = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'sys_action', 'pid = 0 AND hidden=0', '', 'sys_action.sorting');
		} else {
			$groupList = 0;
			if ($GLOBALS['BE_USER']->groupList) {
				$groupList = $GLOBALS['BE_USER']->groupList;
			}
			$queryResource = $GLOBALS['TYPO3_DB']->exec_SELECT_mm_query(
				'sys_action.*',
				'sys_action',
				'sys_action_asgr_mm',
				'be_groups',
				' AND be_groups.uid IN (' . $groupList . ') AND sys_action.pid = 0 AND sys_action.hidden = 0',
				'sys_action.uid',
				'sys_action.sorting'
			);
		}

		if ($queryResource) {
			while ($actionRow = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($queryResource)) {
					
					// Try to fecth icon if it is a domain model object
				if ($actionRow['t3_tables'] != '') {
					$icon = \TYPO3\CMS\Backend\Utility\IconUtility::getSpriteIconForRecord($actionRow['t3_tables'], $actionRow);
				} else {
					$icon = \TYPO3\CMS\Backend\Utility\IconUtility::getSpriteIconForRecord('sys_action', $actionRow);       
				}

					// Override list action and edit action
				if ($actionRow['type'] == 3 || $actionRow['type'] == 5 || $actionRow['type'] == 4) {
					$actions[] = array(
						'title' => $actionRow['title'],
						'uri' => $this->uriBuilder->uriFor('task', array(
							'show' => $actionRow['uid']
						)),
						'icon' => $icon
					);
				} else {
					$actions[] = array(
						'title' => $actionRow['title'],
						'uri' => 'mod.php?M=user_task&SET[mode]=tasks&SET[function]=sys_action.TYPO3\\CMS\\SysAction\\ActionTask&show=' . $actionRow['uid'],
						'icon' => $icon
					);
				}
			}
			$GLOBALS['TYPO3_DB']->sql_free_result($queryResource);
		}
		return $actions;
	}

	/**
	 * Gets the recently changed items
	 *
	 * @param string $where      Additional where conditions
	 * @param int    $queryLimit Limit number of results
	 * @return array             Recently changed items
	 */
	protected function getRecentlyChanged($where = '', $queryLimit = 10) {
		if (!is_int($queryLimit)) {
			$queryLimit = 10;
		}
		$editPermsClause = $GLOBALS['BE_USER']->getPagePermsClause(2);
		$logWhere = ' AND sys_log.event_pid>0 AND sys_log.type=1 AND sys_log.action=2 AND sys_log.error=0';
		$queryResource = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'sys_log.*, max(sys_log.tstamp) AS tstamp_MAX',
			'sys_log,pages',
			'pages.uid=event_pid' .
			' AND' . $editPermsClause .
			$where .
			$logWhere .
			' AND pages.module=\'\'' .
			' AND pages.doktype < 200',
				// Show each page only once
			'sys_log.event_pid',
			'tstamp_MAX DESC',
			$queryLimit
		);

		if ($queryResource) {
			while ($log = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($queryResource)) {
					// Get the record that was changed
				$row = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord(
					$log['tablename'], $log['recuid'], '*', '', FALSE
				);
					// Get permissions for the page where change happened
				if ($log['tablename'] == 'pages') {
					$localCalcPerms = $GLOBALS['BE_USER']->calcPerms($row);
					$permsEdit = $localCalcPerms & 2;
					$page = $row;
				} else {
					$page = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('pages',$row['pid']);
					$localCalcPerms = $GLOBALS['BE_USER']->calcPerms($page);
					$permsEdit = $localCalcPerms & 16;
				}

				if ($permsEdit && $GLOBALS['BE_USER']->isInWebMount($page['uid'], $localCalcPerms)) {
					$iconAltText = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecordIconAltText($row, $log['tablename']);
					$params = '&edit[' . $log['tablename'] . '][' . $row['uid'] . ']=edit';
					$icon = \TYPO3\CMS\Backend\Utility\IconUtility::getSpriteIconForRecord($log['tablename'], $row, array('title' => htmlspecialchars($iconAltText)));
					$title = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecordTitle($log['tablename'], $row);
						// We will show page, but onclick we will edit changed element! So smart
					$returnUrl = $this->uriBuilder->uriFor('home');
					$link = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_DIR') . $GLOBALS['BACK_PATH'] . 'alt_doc.php?returnUrl=' . rawurlencode($returnUrl) . '&edit[' . $log['tablename'] . '][' . intval($row['uid']) . ']=edit';
						
					$items[] = array(
						'uri' => $link,
						'deleted' => $row['deleted'],
						/*'onclick' => htmlspecialchars(\TYPO3\CMS\Backend\Utility\BackendUtility::editOnClick($params, $this->doc->backPath)),*/
						'icon' => $icon,
						'title' => $title,
					);
				}
			}
			$GLOBALS['TYPO3_DB']->sql_free_result($queryResource);
		}
		return $items;
	}

	/**
	 * Loads feed and cuts unneeded items
	 * 
	 * @param  string $feedName The name of the ts setup ( module.tx_pxadashboard.settings.controlPanel.rssFeeds.xxx)
	 * @return array Array from xml
	 */
	protected function getFeed($feedName) {

		$data = array();
			// Get RSS feeds
		$feedConf = $this->settings['controlPanel']['rssFeeds'][$feedName];
		if (isset($feedConf)) {

				// Parse long & short setup versions
			if (is_array($feedConf)) {
				$feedUrl = $feedConf['url'];
				$feedTitle = $feedConf['title'];
				$feedLimit = $feedConf['limit'];
			} elseif (is_string($feedConf)) {
				$feedUrl = $feedConf;
			}

				// Defualt feedLimit
			if (!is_numeric($feedLimit)) {
				$feedLimit = 3;
			} else {
				$feedLimit = (int) $feedLimit;
			}

			try {
					// $feed = $this->getFeed($feedUrl, $feedLimit);
				$request = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
					'TYPO3\\CMS\\Core\\Http\\HttpRequest',
					$feedUrl,
					\TYPO3\CMS\Core\Http\HttpRequest::METHOD_GET,
					array(
					'timeout' => $feedTimeout,
					'follow_redirects' => true
				));
				$result = $request->send();
				$content = $result->getBody();
				$data = json_decode(json_encode((array) simplexml_load_string($content)), 1);

				if (is_int($feedLimit)) {
					$data['channel']['item'] = array_splice($data['channel']['item'], 0, $feedLimit);
				}

				if (!empty($feedTitle)) {
					$data['channel']['title'] = $feedTitle;
				}

			} catch(HTTP_Request2_MessageException $e) {
					// If we timeout, just move on
				continue;
			}
		}

		return $data;
	}

	/**
	 * Action to edit records
	 *
	 * @param array $record sys_action record
	 * @return string list of records
	 */
	protected function getEditContentUri($record) {
		$dbAnalysis = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Database\\RelationHandler');
		$dbAnalysis->setFetchAllFields(TRUE);
		$dbAnalysis->start($record['t4_recordsToEdit'], '*');
		$dbAnalysis->getFromDB();
			// collect the records

		foreach ($dbAnalysis->itemArray as $el) {
			$returnUrl = $this->uriBuilder->uriFor('home');
			$actionList[$el['id']] = array(
				'link' => $GLOBALS['BACK_PATH'] . 'alt_doc.php?returnUrl=' . rawurlencode($returnUrl) . '&edit[' . $el['table'] . '][' . $el['id'] . ']=edit'
			);
		}
			// Return link
		return $actionList[$el['id']]['link'];
	}
}
?>